variables:
  # Store everything under the /builds directory. This is a separate Docker
  # volume. Note that GitLab CI will only cache stuff inside the build
  # directory.
  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/cache"
  GET_SOURCES_ATTEMPTS: 3
  BST_CACHE_SERVER_ADDRESS: 'freedesktop-sdk-cache.codethink.co.uk'
  BST_RELEASES_SERVER_ADDRESS: 'cache.sdk.freedesktop.org'
  BST_SHA: '10abe77fe8d77385d86f225b503d9185f4ef7f3a' #  1.2.3
  BST_EXTERNAL_SHA: '0.9.0-0-g63a19e8068bd777bd9cd59b1a9442f9749ea5a85'
  RUNTIME_VERSION: '18.08'

  # Docker Images
  DOCKER_REGISTRY: "registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images"
  DOCKER_AMD64: "${DOCKER_REGISTRY}/amd64:e132e7bf9180b30c6ad0f4b057442cc2f2a0aa86"
  DOCKER_AARCH64: "${DOCKER_REGISTRY}/aarch64:e132e7bf9180b30c6ad0f4b057442cc2f2a0aa86"

  # Generic variable for invoking buildstream
  BST: bst --colors

stages:
  - update
  - flatpak
  - vm
  - publish

before_script:
  - export PATH=~/.local/bin:${PATH}
  - git clone https://gitlab.com/BuildStream/buildstream.git
  - git -C buildstream checkout "${BST_SHA}"
  - pip3 install --user buildstream/
  - git clone https://gitlab.com/BuildStream/bst-external.git
  - git -C bst-external checkout "${BST_EXTERNAL_SHA}"
  - pip3 install --user ./bst-external

  # Configure Buildstream
  - mkdir -p ~/.config
  - |
    cat > ~/.config/buildstream.conf << EOF
    # Get a lot of output in case of errors
    logging:
      error-lines: 80
    EOF

  # Create ~/.ssh for storing keys
  - mkdir -p ~/.ssh

  # Private key stored as a protected variable that allows pushing to
  # cache.sdk.freedesktop.org
  - |
    if [ -n "$freedesktop_ostree_cache_private_key" ]; then
        echo "$freedesktop_ostree_cache_private_key" > ~/.ssh/id_rsa
        chmod 600 ~/.ssh/id_rsa
        ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub
    fi

  # Create CAS directory for SSL keys
  - mkdir -p /etc/ssl/CAS

  # Private SSL keys/certs for pushing to the CAS server
  - |
    if [ -n "$GITLAB_CAS_PUSH_CERT" ] && [ -n "$GITLAB_CAS_PUSH_KEY" ]; then
        echo "$GITLAB_CAS_PUSH_CERT" > /etc/ssl/CAS/server.crt
        echo "$GITLAB_CAS_PUSH_KEY" > /etc/ssl/CAS/server.key

        echo "projects:" >> ~/.config/buildstream.conf
        echo "  freedesktop-sdk-bootstrap:" >> ~/.config/buildstream.conf
        echo "    artifacts:" >> ~/.config/buildstream.conf
        if [ -f /cache-certificate/server.crt ]; then
            echo "    - url: https://local-cas-server:1102" >> ~/.config/buildstream.conf
            echo "      client-key: /etc/ssl/CAS/server.key" >> ~/.config/buildstream.conf
            echo "      client-cert: /etc/ssl/CAS/server.crt" >> ~/.config/buildstream.conf
            echo "      server-cert: /cache-certificate/server.crt"  >> ~/.config/buildstream.conf
            echo "      push: true" >> ~/.config/buildstream.conf
        fi
        echo "    - url: https://${BST_CACHE_SERVER_ADDRESS}:11002" >> ~/.config/buildstream.conf
        echo "      client-key: /etc/ssl/CAS/server.key" >> ~/.config/buildstream.conf
        echo "      client-cert: /etc/ssl/CAS/server.crt" >> ~/.config/buildstream.conf
        echo "      push: true" >> ~/.config/buildstream.conf
        echo "  freedesktop-sdk:" >> ~/.config/buildstream.conf
        echo "    artifacts:" >> ~/.config/buildstream.conf
        if [ -f /cache-certificate/server.crt ]; then
            echo "    - url: https://local-cas-server:1102" >> ~/.config/buildstream.conf
            echo "      client-key: /etc/ssl/CAS/server.key" >> ~/.config/buildstream.conf
            echo "      client-cert: /etc/ssl/CAS/server.crt" >> ~/.config/buildstream.conf
            echo "      server-cert: /cache-certificate/server.crt"  >> ~/.config/buildstream.conf
            echo "      push: true" >> ~/.config/buildstream.conf
        fi
        echo "    - url: https://${BST_CACHE_SERVER_ADDRESS}:11002" >> ~/.config/buildstream.conf
        echo "      client-key: /etc/ssl/CAS/server.key" >> ~/.config/buildstream.conf
        echo "      client-cert: /etc/ssl/CAS/server.crt" >> ~/.config/buildstream.conf
        echo "      push: true" >> ~/.config/buildstream.conf
    fi

# Store all the downloaded git and ostree repos in the distributed cache.
# This saves us fetching them on every build
.gitlab_cache_template_pull: &gitlab_cache_pull
  cache:
    key: bst
    paths:
      - "${XDG_CACHE_HOME}/buildstream/sources/"
    policy: pull

.gitlab_cache_template_pull_push: &gitlab_cache_pull_push
  cache:
    key: bst
    paths:
      - "${XDG_CACHE_HOME}/buildstream/sources/"



check_update_elements:
  image: $DOCKER_AMD64
  stage: update
  tags:
    - x86_64
    - cache_x86_64
  script:
    - ${BST} track --deps all all.bst
    - git diff
  only:
    - schedules


.flatpak_template: &flatpak_definition
  stage: flatpak
  script:
    - make ARCH=${ARCH} build
    - make ARCH=${ARCH} check-dev-files

    - export FLATPAK_USER_DIR="${PWD}/tmp-flatpak"
    - make ARCH=${ARCH} export
    - make ARCH=${ARCH} test-apps

    - |
      REFERENCE=$(git merge-base ${RUNTIME_VERSION} ${CI_COMMIT_SHA}) && \
      ./utils/check-abi --old=${REFERENCE} --new=${CI_COMMIT_SHA} abi/desktop-abi-image.bst


  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
    - '18.08'
  <<: *gitlab_cache_pull

app_x86_64:
  image: $DOCKER_AMD64
  <<: *flatpak_definition
  tags:
    - x86_64
    - cache_x86_64
  variables:
    ARCH: x86_64

app_i686:
  image: $DOCKER_AMD64
  <<: *flatpak_definition
  tags:
    - x86_64
    - cache_i686
  variables:
    ARCH: i686

app_aarch64:
  image: $DOCKER_AARCH64
  <<: *flatpak_definition
  tags:
    - aarch64
  variables:
    ARCH: aarch64

app_arm:
  image: $DOCKER_AARCH64
  <<: *flatpak_definition
  tags:
    - armhf
  variables:
    ARCH: arm


.vm_image_template: &vm_image
  stage: vm
  script:
    - ${BST} -o target_arch "${ARCH}" build vm/"${TYPE}"-vm-image-"${ARCH}".bst
    - ${BST} -o target_arch "${ARCH}" checkout vm/"${TYPE}"-vm-image-"${ARCH}".bst ./vm
    - utils/test-minimal-system --dialog "${DIALOG}" vm/sda.img
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
    - '18.08'
  <<: *gitlab_cache_pull

minimal_vm_image_x86_64:
  image: $DOCKER_AMD64
  tags:
    - x86_64
    - cache_x86_64
  <<: *vm_image
  variables:
    ARCH: x86_64
    TYPE: minimal
    DIALOG: minimal

minimal_systemd_vm_image_x86_64:
  image: $DOCKER_AMD64
  tags:
    - x86_64
    - cache_x86_64
  <<: *vm_image
  variables:
    ARCH: x86_64
    TYPE: minimal-systemd
    DIALOG: root-login

.flatpak_runtimes_publish_template: &flatpak_runtimes_publish
  stage: publish
  script:
    - make ARCH=${ARCH} build
    - make ARCH=${ARCH} export

    # Trust the host key of the release server.
    - ssh-keyscan "${BST_RELEASES_SERVER_ADDRESS}" >> ~/.ssh/known_hosts

    - BRANCHES=$(find repo/refs/heads/ -type f | sed s,repo/refs/heads/,,)

    - rsync -a repo/ releases@${BST_RELEASES_SERVER_ADDRESS}:incoming/repo-${CI_JOB_ID}
    - ssh releases@${BST_RELEASES_SERVER_ADDRESS} import-commits -c /etc/releases-config.json incoming/repo-${CI_JOB_ID} ${BRANCHES}
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  only:
    - master
    - '18.08'
  except:
    - schedules
  <<: *gitlab_cache_pull_push

publish_x86_64:
  image: $DOCKER_AMD64
  <<: *flatpak_runtimes_publish
  tags:
    - x86_64
    - cache_x86_64
  variables:
    ARCH: x86_64

publish_i686:
  image: $DOCKER_AMD64
  <<: *flatpak_runtimes_publish
  tags:
    - x86_64
    - cache_i686
  variables:
    ARCH: i686

publish_aarch64:
  image: $DOCKER_AARCH64
  <<: *flatpak_runtimes_publish
  tags:
    - aarch64
  variables:
    ARCH: aarch64

publish_arm:
  image: $DOCKER_AARCH64
  <<: *flatpak_runtimes_publish
  tags:
    - armhf
  variables:
    ARCH: arm

cve_report:
  stage: publish
  image: $DOCKER_AMD64
  cache:
    key: cve
    paths:
      - "${XDG_CACHE_HOME}/cve"
  tags:
    - x86_64
    - cache_x86_64
  script:
    - pip3 install --user lxml
    - mkdir -p "${XDG_CACHE_HOME}/cve"
    - cd "${XDG_CACHE_HOME}/cve"
    - python3 "${CI_PROJECT_DIR}/utils/update-local-cve-database.py"
    - ${BST} -o target_arch "x86_64" build sdk-manifest.bst platform-manifest.bst
    - mkdir -p "${CI_PROJECT_DIR}/sdk-manifest"
    - mkdir -p "${CI_PROJECT_DIR}/platform-manifest"
    - ${BST} -o target_arch "x86_64" checkout sdk-manifest.bst "${CI_PROJECT_DIR}/sdk-manifest"
    - ${BST} -o target_arch "x86_64" checkout platform-manifest.bst "${CI_PROJECT_DIR}/platform-manifest"
    - mkdir -p "${CI_PROJECT_DIR}/cve-reports"
    - python3 "${CI_PROJECT_DIR}/utils/generate-cve-report.py" "${CI_PROJECT_DIR}/sdk-manifest/usr/manifest.json" "${CI_PROJECT_DIR}/cve-reports/sdk.html"
    - python3 "${CI_PROJECT_DIR}/utils/generate-cve-report.py" "${CI_PROJECT_DIR}/platform-manifest/usr/manifest.json" "${CI_PROJECT_DIR}/cve-reports/platform.html"
  artifacts:
    paths:
      - "${CI_PROJECT_DIR}/cve-reports"
  only:
    - master
    - '18.08'
